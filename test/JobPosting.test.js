const assert = require('assert')
const { describe } = require('mocha')
const { checkEnableTime } = require("../JobPosting")

describe('JobPosting', function () {
  it ('show return when เวลาสมัครอยู่ระหว่างเริ่มต้นและสิ้นสุด', function (){
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true;
    const actualResult = checkEnableTime(startTime, endTime, today)
    assert.strictEqual(actualResult, expectedResult)
  })
  it ('show return when เวลาสมัครอยู่ก่อนเริ่มต้นและสิ้นสุด', function (){
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = true;
    const actualResult = checkEnableTime(startTime, endTime, today)
    assert.strictEqual(actualResult, expectedResult)
  })
  it ('show return when เวลาสมัครอยู่หลังเริ่มต้นและสิ้นสุด', function (){
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 7)
    const expectedResult = true;
    const actualResult = checkEnableTime(startTime, endTime, today)
    assert.strictEqual(actualResult, expectedResult)
  })
  it ('show return when เวลาสมัครอยู่เท่ากับวันเริ่มต้น', function (){
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true;
    const actualResult = checkEnableTime(startTime, endTime, today)
    assert.strictEqual(actualResult, expectedResult)
  })
  it ('show return when เวลาสมัครอยู่เท่ากับวันสิ้นสุด', function (){
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true;
    const actualResult = checkEnableTime(startTime, endTime, today)
    assert.strictEqual(actualResult, expectedResult)
  })
})
